package config.converter;


import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import controller.dto.PersonDTO;
import io.opentelemetry.api.internal.StringUtils;
import org.springframework.core.convert.converter.Converter;

import java.util.ArrayList;
import java.util.List;

public class PersonConverter implements Converter<String, List<PersonDTO>> {
    /**
     * Convert the source object of type {@code S} to target type {@code T}.
     *
     * @param source the source object to convert, which must be an instance of {@code S} (never {@code null})
     * @return the converted object, which must be an instance of {@code T} (potentially {@code null})
     * @throws IllegalArgumentException if the source cannot be converted to the desired target type
     */
    @Override
    public List<PersonDTO> convert(final String source) {
        return StringUtils.isNullOrEmpty(source)
                ? new ArrayList<>()
                : new Gson().fromJson(source, new TypeToken<List<PersonDTO>>(){}.getType());
    }
}
